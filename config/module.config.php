<?php

return array(
    'controller_plugins' => array(
        'invokables' => array(
            'isAllowed' => 'User\Controller\Plugin\IsAllowed',
        )
    ),
    'service_manager' => array(
        'factories' => array(
            'auth' => 'User\Service\Factory\Authentication',
            'laji-auth' => 'User\Service\Factory\LajiAuthentication',
            'acl' => 'User\Service\Factory\Acl',
            'user' => 'User\Service\Factory\User',
            \User\Options\UserAuthOptions::class => \User\Options\UserAuthOptionsFactory::class,
            \LajiAuth\Service\LajiAuthClient::class => \User\Service\Factory\LajiAuthClient::class
        ),
        'invokables' => array(
            'auth-adapter' => 'User\Authentication\Adapter\Ltkm',
            'laji-auth-adapter' => 'User\Authentication\Adapter\LajiAuth',
        )
    ),
    'view_manager' => array(
        'template_map' => include __DIR__ . '/../template_map.php',
    ),
    'view_helpers' => array(
        'invokables' => array(
            'User' => 'User\View\Helper\User',
            'isAllowed' => 'User\View\Helper\IsAllowed',
        )
    )
);
