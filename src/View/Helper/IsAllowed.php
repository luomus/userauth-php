<?php
namespace User\View\Helper;

use Triplestore\Classes\MAPersonInterface;
use Zend\Navigation;
use Zend\Permissions\Acl\Acl;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorAwareTrait;
use Zend\View\Helper\AbstractHelper;

/**
 * Class IsAllowed
 * @package View\Helper
 */
class IsAllowed extends AbstractHelper implements ServiceLocatorAwareInterface
{

    use ServiceLocatorAwareTrait;

    protected $acl;
    protected $user;

    /**
     * @param null $resource
     * @param null $privilege
     * @return $this
     */
    public function __invoke($resource = null, $privilege = null)
    {
        if ($resource === null) {
            return $this;
        }

        return $this->isAllowed($resource, $privilege);
    }

    public function isAllowed($resource, $privilege = null)
    {
        $role = $this->getUser()->getMARoleKotka();
        if (is_array($role)) {
            $role = array_pop($role);
        }
        return $this->getAcl()->isAllowed(
            $role,
            $resource,
            $privilege
        );
    }

    /**
     * @return MAPersonInterface
     */
    public function getUser()
    {
        if ($this->user === null) {
            $this->user = $this->getServiceLocator()->get('user')->getUser();
        }
        return $this->user;
    }

    /**
     * @return Acl
     */
    public function getAcl()
    {
        if ($this->acl === null) {
            $this->acl = $this->getServiceLocator()->getServiceLocator()->get('acl');
        }
        return $this->acl;
    }
}
