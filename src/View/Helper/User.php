<?php

namespace User\View\Helper;

use Triplestore\Classes\MAPersonInterface;
use User\Enum\User as UserEnum;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorAwareTrait;
use Zend\View\Helper\AbstractHelper;

/**
 * Class User is view helper class to return the data about the logged in user
 *
 * @package User\View\Helper
 */
class User extends AbstractHelper implements ServiceLocatorAwareInterface
{
    use ServiceLocatorAwareTrait;

    const USERNAME = 'username';
    const FULLNAME = 'fullname';
    const EMAIL = 'email';

    /** @var MAPersonInterface $user */
    protected $user;

    public function getUser()
    {
        if ($this->user === null) {
            $this->setUser($this->serviceLocator->getServiceLocator()->get('user'));
        }
        return $this->user;
    }

    public function setUser(MAPersonInterface $user)
    {
        $this->user = $user;
        return $this;
    }

    /**
     * Gives the identity when the class is called
     *
     * @param string $type
     * @return string|User
     */
    public function __invoke($type = null)
    {
        if ($type == null) {
            return $this;
        }
        if (!$this->getUser() instanceof MAPersonInterface) {
            return '';
        }
        switch ($type) {
            case self::USERNAME:
                return $this->getString($this->getUser()->getMALTKMLoginName());
            case self::FULLNAME:
                return $this->getString($this->getUser()->getMAInheritedName()) . ', ' . $this->getString($this->getUser()->getMAPreferredName());
            case self::EMAIL:
                return $this->getString($this->getUser()->getMAEmailAddress());
        }
        return '';
    }

    public function isAdmin()
    {
        return $this->isUser(UserEnum::ROLE_ADMIN);
    }

    public function isAdvanced()
    {
        return $this->isUser(UserEnum::ROLE_ADVANCED);
    }

    public function isGuest()
    {
        return $this->isUser(UserEnum::ROLE_GUEST, true);
    }

    private function isUser($role, $whenNoUser = false)
    {
        $user = $this->getUser();
        if ($user === null) {
            return $whenNoUser;
        }
        if ($user->getMARoleKotka() === $role) {
            return true;
        }
        return false;
    }

    private function getString($value)
    {
        if (is_array($value)) {
            return current($value);
        }
        return $value;
    }
}
