<?php
namespace User\Options;


use Zend\Stdlib\AbstractOptions;

class UserAuthOptions extends AbstractOptions
{
    /** @var String */
    private $lajiAuthUri;
    /** @var String */
    private $systemId;
    /** @var String */
    private $userClass;
    /** @var String */
    private $failView;
    /** @var String */
    private $deniedPage = '/';

    private $httpClientOptions = [];

    private $ldap;


    /**
     * @return String
     */
    public function getLajiAuthUri()
    {
        return $this->lajiAuthUri;
    }

    /**
     * @param String $lajiAuthUri
     */
    public function setLajiAuthUri($lajiAuthUri)
    {
        $this->lajiAuthUri = $lajiAuthUri;
    }

    /**
     * @return String
     */
    public function getSystemId()
    {
        return $this->systemId;
    }

    /**
     * @param String $systemId
     */
    public function setSystemId($systemId)
    {
        $this->systemId = $systemId;
    }

    /**
     * @return String
     */
    public function getFailView()
    {
        return $this->failView;
    }

    /**
     * @param String $failView
     */
    public function setFailView($failView)
    {
        $this->failView = $failView;
    }

    /**
     * @return String
     */
    public function getDeniedPage()
    {
        return $this->deniedPage;
    }

    /**
     * @param String $deniedPage
     */
    public function setDeniedPage($deniedPage)
    {
        $this->deniedPage = $deniedPage;
    }

    /**
     * @return array
     */
    public function getHttpClientOptions()
    {
        return $this->httpClientOptions;
    }

    /**
     * @param array $httpClientOptions
     */
    public function setHttpClientOptions($httpClientOptions)
    {
        $this->httpClientOptions = $httpClientOptions;
    }

    /**
     * @return String
     */
    public function getUserClass()
    {
        return $this->userClass;
    }

    /**
     * @param String $userClass
     */
    public function setUserClass($userClass)
    {
        $this->userClass = $userClass;
    }

    /**
     * @return mixed
     */
    public function getLdap()
    {
        return $this->ldap;
    }

    /**
     * @param mixed $ldap
     */
    public function setLdap($ldap)
    {
        $this->ldap = $ldap;
    }
}