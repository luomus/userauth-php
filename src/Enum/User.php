<?php

namespace User\Enum;


abstract class User
{
    const ROLE_GUEST = 'MA.guest';
    const ROLE_MEMBER = 'MA.member';
    const ROLE_ADVANCED = 'MA.advanced';
    const ROLE_ADMIN = 'MA.admin';

    const GUEST_ORGANIZATION = 'MOS.0';
}