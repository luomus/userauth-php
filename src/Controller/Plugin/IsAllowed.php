<?php
namespace User\Controller\Plugin;

use Triplestore\Classes\MAPersonInterface;
use Zend\Mvc\Controller\Plugin\AbstractPlugin;
use Zend\Permissions\Acl\Acl;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorAwareTrait;

class IsAllowed extends AbstractPlugin implements ServiceLocatorAwareInterface
{
    use ServiceLocatorAwareTrait;

    protected $acl;
    protected $user;

    /**
     * @param null $resource
     * @param null $privilege
     * @return $this
     */
    public function __invoke($resource = null, $privilege = null)
    {
        if ($resource === null) {
            return $this;
        }

        return $this->isAllowed($resource, $privilege);
    }

    public function isAllowed($resource, $privilege = null)
    {
        return $this->getAcl()->isAllowed(
            $this->getUser()->getMARoleKotka(),
            $resource,
            $privilege
        );
    }

    /**
     * @return MAPersonInterface
     */
    public function getUser()
    {
        if ($this->user === null) {
            $this->user = $this->getServiceLocator()->get('user');
        }
        return $this->user;
    }

    /**
     * @return Acl
     */
    public function getAcl()
    {
        if ($this->acl === null) {
            $this->acl = $this->getServiceLocator()->get('acl');
        }
        return $this->acl;
    }

}