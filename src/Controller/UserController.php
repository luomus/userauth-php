<?php

namespace User\Controller;

use LajiAuth\Model\AuthenticationSources;
use LajiAuth\Service\LajiAuthClient;
use Triplestore\Classes\Hydrator\MAPersonDb;
use Triplestore\Classes\MAPersonInterface;
use User\Authentication\Adapter\LajiAuth;
use User\Authentication\Adapter\Ltkm;
use Zend\Authentication\Adapter\ValidatableAdapterInterface;
use Zend\Authentication\AuthenticationService;
use Zend\Authentication\Result;
use Zend\EventManager\EventManager;
use Zend\Http\Request;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\Session\Container;
use Zend\View\Model\JsonModel;
use Zend\View\Model\ViewModel;

/**
 * Class UserController
 * @package User\Controller
 */
class UserController extends AbstractActionController
{
    const REDIRECT_CONTAINER_NAME = 'redirectContainer';

    public function indexAction()
    {
        /** @var AuthenticationService $auth */
        $auth = $this->serviceLocator->get('auth');
        if ($auth->hasIdentity()) {
            return $this->redirect()->toUrl('/');
        }
        /** @var LajiAuthClient $client */
        $client = $this->getServiceLocator()->get(LajiAuthClient::class);
        $this->layout()->setVariable('skip_init', true);
        /** @var Request $request */
        $request = $this->getRequest();;
        $view = new ViewModel();
        $view->setTemplate('user/login');
        $loginUrl = $this->url()->fromRoute('https/user',['action' => 'LajiAuthlogin']);
        $view->setVariable('virtu',$client->createLoginUrl($loginUrl, [], AuthenticationSources::VIRTU)->build());
        $view->setVariable('haka', $client->createLoginUrl($loginUrl, [], AuthenticationSources::HAKA)->build());
        $view->setVariable('laji', $client->createLoginUrl($loginUrl, [], AuthenticationSources::LOCAL)->build());

        if (!$request->isPost()) {
            return $view;
        }

        $username = $request->getPost('username');
        $password = $request->getPost('password');
        $keep = $request->getPost('keep', false);

        /** @var AuthenticationService $auth */
        $auth = $this->serviceLocator->get('auth');
        /** @var Ltkm $authAdapter */
        $authAdapter = $auth->getAdapter();
        $authAdapter->setIdentity($username);
        $authAdapter->setCredential($password);

        $result = $auth->authenticate();
        $isValid = $result->isValid();
        if ($isValid) {
            $event = new EventManager('login');
            /** @var \Zend\Session\SessionManager $sessionManager */
            if ($keep) {
                $sessionManager = $this->serviceLocator->get('Zend\Session\SessionManager');
                $sessionManager->rememberMe();
            }
            $event->trigger('success', null, array('username' => $username));
            $container = new Container(self::REDIRECT_CONTAINER_NAME);
            $redirect = $container->redirect ?: '/';
            // redirect to user index page or to page that required login
            return $this->redirect()->toUrl($redirect);
        }
        $errorMessages = $result->getMessages();
        $view->setVariable('errorMessage', array_shift($errorMessages));

        return $view;
    }

    /**
     * Handles the user login attempt
     * Shows the login form if request was not post
     *
     * @return \Zend\Http\Response|ViewModel
     */
    public function LajiAuthLoginAction()
    {
        /** @var Request $request */
        $request = $this->getRequest();
        /** @var LajiAuthClient $client */
        $client = $this->getServiceLocator()->get(LajiAuthClient::class);
        if (!$request->isPost()) {
            return $this->redirect()->toUrl($client->createLoginUrl('/user/login')->build());
        }
        $token = $request->getPost('token', []);
        /** @var AuthenticationService $auth */
        $auth = $this->serviceLocator->get('laji-auth');
        /** @var LajiAuth $authAdapter */
        $authAdapter = $auth->getAdapter();
        $authAdapter->setCredential($token);

        /** @var Result $result */
        $result = $auth->authenticate();
        $isValid = $result->isValid();
        if ($isValid) {
            $user = $result->getIdentity();
            $username = $user instanceof MAPersonInterface ? $user->getSubject() : 'unknown';
            $event = new EventManager('login');
            $event->trigger('success', null, array('username' => $username));
            $container = new Container(self::REDIRECT_CONTAINER_NAME);
            $redirect = $container->redirect ?: '/';
            // redirect to user index page or to page that required login
            return $this->redirect()->toUrl($redirect);
        }
        $errorMessages = $result->getMessages();
        $this->flashMessenger()->addErrorMessage(array_shift($errorMessages));
        return $this->redirect()->toRoute('https/user');
    }

    /**
     * This enables remote user authentication
     *
     * @return JsonModel
     */
    public function remoteAction()
    {
        $view = new JsonModel();
        /** @var Request $request */
        $request = $this->getRequest();
        $return = array(
            'success' => false,
        );
        if ($request->isPost()) {
            $username = $request->getPost('username');
            $password = $request->getPost('password');
            /** @var AuthenticationService $auth */
            $auth = $this->serviceLocator->get('auth');
            /** @var ValidatableAdapterInterface $authAdapter */
            $authAdapter = $auth->getAdapter();
            $authAdapter->setIdentity($username);
            $authAdapter->setCredential($password);
            if ($authAdapter instanceof Ltkm) {
                $authAdapter->setAllowGenericRole(true);
            }
            if (empty($username) || empty($password)) {
                $return['message'] = 'You must enter both username and password.';
            }
            $result = $auth->authenticate();
            if ($result->isValid()) {
                $return['success'] = true;
                $user = $auth->getIdentity();
                if ($user instanceof MAPersonInterface) {
                    $hydrator = new MAPersonDb();
                    $return['identity'] = $hydrator->extract($user);
                } else {
                    $return['message'] = 'Could not find the user!';
                    $return['code'] = '404';
                }
            } else {
                $messages = $result->getMessages();
                $messages = array_shift($messages);
                $return['message'] = $messages;
                $return['code'] = $result->getCode();
            }
            $result->isValid();
        }
        $view->setVariables($return);
        return $view;
    }

    /**
     * Handles user logout action
     * @return \Zend\Http\Response
     */
    public function logoutAction()
    {
        $auth = $this->serviceLocator->get('auth');
        $auth->clearIdentity();
        $redirectUrl = '/';
        $request = $this->getRequest();
        if ($request instanceof Request) {
            /** @var \Zend\Uri\Uri $redirectUrl */
            $header = $request->getHeader('Referer', $redirectUrl);
            if ($header instanceof \Zend\Http\Header\Referer) {
                $redirectUrl = $header->uri();
                if (strpos($redirectUrl->getPath(), '/user') !== false) {
                    $redirectUrl->setPath('/');
                }
            }
        }
        try{
            $container = new Container(LajiAuth::LAJIAUTH);
            if ($container->token) {
                /** @var LajiAuthClient $client */
                $client = $this->getServiceLocator()->get(LajiAuthClient::class);
                $client->deleteToken($container->token);
                $container->offsetUnset('token');
            }
        } catch (\Exception $e) {
        }


        return $this->redirect()->toUrl($redirectUrl);
    }
}
