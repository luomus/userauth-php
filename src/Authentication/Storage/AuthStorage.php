<?php
namespace User\Authentication\Storage;

use Zend\Authentication\Storage\Session;

/**
 * Class AuthStorage is storage that is used to store authentication data
 * @package User\Authentication\Storage
 */
class AuthStorage extends Session
{
}
