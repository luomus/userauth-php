<?php

namespace User\Authentication\Adapter;

use Triplestore\Classes\MAPersonInterface;
use User\Enum\User;
use Common\Service\NameService;
use Zend\Authentication\Adapter\AbstractAdapter;
use Zend\Authentication\Adapter\Ldap;
use Zend\Authentication\Result;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorAwareTrait;

/**
 * Class Ltkm is authentication adapter that wraps the ldap and triplestore in one
 * @package User\Authentication\Adapter
 */
class Ltkm extends AbstractAuth
{
    private $testUsersOrganizations = [
        'MOS.4662' => 'MOS.4662'
    ];

    private $skipLdap = false;
    private $allowGenericRole = false;

    /**
     * Authenticate the user
     *
     * @return Result
     * @throws \Exception
     */
    public function authenticate()
    {
        $config = $this->serviceLocator->get('Config');
        if (isset($config['test']) && isset($config['test']['login'])) {
            $user = isset($config['test']['login']['username']) ? $config['test']['login']['username'] : null;
            $pass = isset($config['test']['login']['password']) ? $config['test']['login']['password'] : null;
            if (
                $this->identity === $user
                && $this->credential === $pass
            ) {
                return $this->testUserLogin($config);
            }
        }

        if (!isset($config['auth'])) {
            throw new \Exception('Missing authentication configuration');
        }
        $auth = $config['auth'];
        $ldapOptions = $auth['ldap'];
        $acl = $config['acl'];
        $ldapAdapter = null;

        if (!$this->skipLdap) {
            $ldapAdapter = new Ldap($ldapOptions);
            $ldapAdapter->setIdentity($this->identity);
            $ldapAdapter->setCredential($this->credential);

            $ldapAuthResult = $ldapAdapter->authenticate();
            // If not successfull just return the ldap result
            if ($ldapAuthResult->getCode() !== Result::SUCCESS) {
                return $ldapAuthResult;
            }
        }

        /** @var \Triplestore\Service\ObjectManager $om */
        $om = $this->serviceLocator->get('Triplestore\ObjectManager');
        try {
            $om->enableHydrator();
            $user = $om->findBy(array('MA.LTKMLoginName' => $this->identity));
            if ($user !== null && count($user) === 1) {
                $user = current($user);
                if ($user instanceof MAPersonInterface) {
                    if (!$this->allowGenericRole && empty($user->getMARoleKotka())) {
                        return new Result(Result::FAILURE_IDENTITY_NOT_FOUND, $user, array("You don't have access to Kotka.<br>(No Kotka role in triplestore)"));
                    }
                    $this->setOrganization($ldapAdapter);
                    return $this->returnSuccessResult($user, $acl);
                }
                return new Result(Result::FAILURE_IDENTITY_NOT_FOUND, $user, array('Could not find user'));
            } else if ($user === null || count($user) == 0) {
                return new Result(Result::FAILURE_IDENTITY_NOT_FOUND, $this->identity, array('Username not found. Please note that Luomus usernames are all lowercase.<br>'
                    . 'Käyttäjätunnusta ei löytynyt. Huomaa että Luomuksen käyttäjätunnukset kirjoitetaan kokonaan pienin kirjaimin.<br>'
                    . '(User not in triplestore)'));
            } else if (count($user) > 1) {
                return new Result(Result::FAILURE_IDENTITY_AMBIGUOUS, $this->identity, array('Ambiguous username given'));
            }
        } catch (\Exception $e) {
            return new Result(
                Result::FAILURE, $this->identity, array('Could not contact the authentication service. Place wait and try again later. If the problem continues, contact Kotka administrators')
            );
        }
        $user = current($user);
        return new Result(Result::FAILURE, $user, array('Unable to authenticate the user'));
    }

    /**
     * This is intended to enable user login from commandline and it should not be used from anywhere else!
     */
    public function _skipLdapAuthentication($organization = null)
    {
        $this->skipLdap = true;
        if (is_string($organization)) {
            $organization = [$organization => $organization];
        }
        $this->organizations = $organization;
    }

    /**
    * @return bool
    */
    public function isAllowGenericRole()
    {
        return $this->allowGenericRole;
    }

    /**
     * @param bool $allowGenericRole
     */
    public function setAllowGenericRole($allowGenericRole)
    {
        $this->allowGenericRole = $allowGenericRole;
    }

    /**
     * Extracts the user groups from memberof ldap array
     * @param array $memberof
     * @return array
     */
    private function extractUserGroups(array $memberof)
    {
        $userGroups = array();
        foreach ($memberof as $key => $value) {
            if ($key !== "count") {
                $pieces = explode(",", $value);
                $value = $pieces[0];
                $value = str_replace('CN=', '', $value);
                $userGroups[$key] = $value;
            }
        }

        return $userGroups;
    }

    private function setOrganization($ldapAdapter)
    {
        if (!method_exists($ldapAdapter, 'getAccountObject')) {
            return;
        }

        $accountLdap = $ldapAdapter->getAccountObject(array('memberof'));
        $organizations = array();
        if (property_exists($accountLdap, 'memberof')) {
            $memberOf = (array)$accountLdap->memberof;
            $memberOf = (array)$this->extractUserGroups($memberOf);
            /** @var \Triplestore\Service\ObjectManager $om */
            $om = $this->serviceLocator->get('Triplestore\ObjectManager');
            // Populate the user in organization data
            foreach ($memberOf as $group) {
                $groups = $om->findBy(array('MOS.ad' => $group));
                $count = count($groups);
                if ($count == 1) {
                    $organization = current($groups);
                    $organizations[] = $organization->getSubject();
                } else if ($count > 1) {
                    /** @var \Zend\Log\Logger $logger */
                    $logger = $this->serviceLocator->get('Logger');
                    $logger->crit(sprintf('Ad group "%s" used in multiple organizations (%s)!',
                        $group,
                        implode(', ', array_keys($groups))));
                }
            }
        }
        $this->organizations = $organizations;
    }

    private function testUserLogin($config)
    {
        $user = null;
        if (isset($config['auth']) && isset($config['auth']['userClass'])) {
            $class = $config['auth']['userClass'];
            $user = new $class();
            if ($user instanceof MAPersonInterface) {
                $user->setMARole([User::ROLE_MEMBER]);
                $user->setMARoleKotka(User::ROLE_MEMBER);
                $user->setMALTKMLoginName($this->identity);
                $user->setSubject('MA.007');
                $user->setMAEmailAddress('test@kotka.luomus.fi');
                $user->setMAFullName('Test User');
                $user->setMAGivenNames('Test');
                $user->setMAInheritedName('User');
                $user->setMAPreferredName('Test');
                $user->setMAYearOfBirth('2011');
                $organizations = $this->testUsersOrganizations;
                foreach ($organizations as $key => $value) {
                    $organizations[$key] = "Test Users organization " . $key;
                }
                $user->setMAOrganisation($organizations);
            }
        }

        return new Result(Result::SUCCESS, $user, array('authentication successful'));
    }
}
