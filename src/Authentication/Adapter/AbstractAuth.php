<?php

namespace User\Authentication\Adapter;

use Common\Service\IdService;
use Common\Service\NameService;
use Triplestore\Classes\MAPersonInterface;
use Triplestore\Classes\MOSOrganizationInterface;
use Zend\Authentication\Adapter\AbstractAdapter;
use Zend\Authentication\Result;
use Zend\Log\LoggerInterface;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorAwareTrait;

abstract class AbstractAuth extends AbstractAdapter implements ServiceLocatorAwareInterface
{
    use ServiceLocatorAwareTrait;

    protected $organizations = [];

    protected function returnSuccessResult(MAPersonInterface $user, $acl)
    {
        $default = $user->getMADefaultQNamePrefix();
        if (!empty($default) && !IdService::isValidDefaultQNamePrefix($default)) {
            if ($this->getServiceLocator()->has('logger')) {
                /** @var LoggerInterface $logger */
                $logger = $this->getServiceLocator()->get('logger');
                $logger->crit('User ' . $user->getSubject() . ' has invalid qname prefix');
            }
            return new Result(Result::FAILURE, null, array('Invalid default domain. Please contact the Kotka support!'));
        } else if (empty($default)) {
            $user->setMADefaultQNamePrefix(rtrim(IdService::QNAME_PREFIX_LUOMUS,':'));
        }
        $this->setUserOrganization($user);
        $this->checkUserRole($user, $acl);
        $this->setUserService($user);
        return new Result(Result::SUCCESS, $user, array('authentication successful'));
    }

    protected function setUserService(MAPersonInterface $user)
    {
        $this->getServiceLocator()
            ->setAllowOverride(true)
            ->setService('user', $user)
            ->setAllowOverride(false);
    }

    protected function checkUserRole(MAPersonInterface $user, $acl)
    {
        $roles = $acl['role'];
        $role = $user->getMARoleKotka();
        $default = $acl['defaults']['member_role'];
        if (!array_key_exists($role, $roles)) {
            $user->setMARoleKotka($default);
        }
    }

    protected function setUserOrganization(MAPersonInterface $user)
    {
        $organizations = $user->getMAOrganisation();
        if (!is_array($organizations) || empty($organizations)) {
            $user->setMAOrganisation($this->getOrganizationNames($this->organizations));
            return;
        }
        $user->setMAOrganisation($this->getOrganizationNames($organizations));
    }

    private function getOrganizationNames($organizations) {
        if (!is_array($organizations)) {
            return [];
        }
        /** @var \Triplestore\Service\ObjectManager $om */
        $om = $this->serviceLocator->get('Triplestore\ObjectManager');
        $organizations = $om->fetch($organizations);
        $nameService = new NameService();
        $namedOrganizations = [];
        /** @var MOSOrganizationInterface $organization */
        foreach($organizations as $organization) {
            $namedOrganizations[$organization->getSubject()] = $nameService->getOrganizationName($organization);
        }
        return $namedOrganizations;
    }

}