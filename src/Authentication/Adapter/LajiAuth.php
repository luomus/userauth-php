<?php

namespace User\Authentication\Adapter;

use LajiAuth\Service\LajiAuthClient;
use Triplestore\Classes\MAPersonInterface;
use Zend\Authentication\Result;
use Zend\Session\Container;

/**
 * Class Ltkm is authentication adapter that wraps the ldap and triplestore in one
 * @package User\Authentication\Adapter
 */
class LajiAuth extends AbstractAuth
{
    const LAJIAUTH = 'LajiAuth';

    /**
     * Authenticate the user
     *
     * @return Result
     * @throws \Exception
     */
    public function authenticate()
    {
        /** @var LajiAuthClient $client */
        $client = $this->getServiceLocator()->get(LajiAuthClient::class);
        $config = $this->serviceLocator->get('Config');
        $token = $this->credential;
        try {
            $info = $client->getTokenInfo($token);
        } catch (\Exception $e) {
            return new Result(Result::FAILURE, $e->getCode(), array($e->getMessage()));
        }
        if (
            !isset($info['user']['qname']) ||
            $info['target'] !== $config['auth']['systemId']
        ) {
            return new Result(Result::FAILURE, null, array('Unable to authenticate the user'));
        }

        /** @var \Triplestore\Service\ObjectManager $om */
        $om = $this->serviceLocator->get('Triplestore\ObjectManager');
        try {
            $om->enableHydrator();
            $user = $om->fetch($info['user']['qname']);
            if ($user instanceof MAPersonInterface) {
                $container = new Container(self::LAJIAUTH);
                $container->token = $token;
                $acl = $config['acl'];
                return $this->returnSuccessResult($user, $acl);
            }
        } catch (\Exception $e) {
            return new Result(
                Result::FAILURE, null, array('Could not contact the authentication service. Place wait and try again later. If the problem continues, contact Kotka administrators')
            );
        }
        return new Result(Result::FAILURE, null, array('No MA account found in the database!'));
    }

}
