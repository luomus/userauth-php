<?php
namespace User\Service\Factory;

use User\Options\UserAuthOptions;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\Uri\Http as HttpUri;

class LajiAuthClient implements FactoryInterface
{
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        /** @var UserAuthOptions $options */
        $options = $serviceLocator->get(UserAuthOptions::class);
        return new \LajiAuth\Service\LajiAuthClient(
            $options->getSystemId(),
            new HttpUri($options->getLajiAuthUri()),
            $options->getHttpClientOptions()
        );
    }
}
