<?php
namespace User\Service\Factory;

use Zend\Authentication\AuthenticationService;
use Zend\Authentication\Storage\Session;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class LajiAuthentication implements FactoryInterface
{

    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $adapter = $serviceLocator->get('laji-auth-adapter');
        $storage = new Session('auth_storage');
        $auth = new AuthenticationService($storage, $adapter);

        return $auth;
    }
}
