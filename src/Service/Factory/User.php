<?php
namespace User\Service\Factory;

use Triplestore\Classes\MAPersonInterface;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class User implements FactoryInterface
{
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $config = $serviceLocator->get('config');
        $auth = $serviceLocator->get('auth');
        $user = null;
        if (method_exists($auth, 'hasIdentity') && $auth->hasIdentity()) {
            /** @var MAPersonInterface $user */
            $user = $auth->getIdentity();
            if ($user instanceof MAPersonInterface) {
                if (!$user->getMARoleKotka()) {
                    $user->setMARoleKotka($config['acl']['defaults']['member_role']);
                }
            }
        } else if (isset($config['auth']) && isset($config['auth']['userClass'])) {
            $class = $config['auth']['userClass'];
            $user = new $class();
            if ($user instanceof MAPersonInterface) {
                $user->setMARoleKotka($config['acl']['defaults']['guest_role']);
                $user->setMAOrganisation(['MOS.0' => 'undefined organization']);
                $user->setMADefaultQNamePrefix('tun');
            }
        }

        return $user;
    }
}
