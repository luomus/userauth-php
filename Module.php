<?php

namespace User;

use Triplestore\Classes\MAPersonInterface;
use User\Controller\UserController;
use User\Enum\User;
use Zend\Authentication\AuthenticationService;
use Zend\Console\Console;
use Zend\EventManager\EventManager;
use Zend\Http\Response;
use Zend\ModuleManager\Feature\ConfigProviderInterface;
use Zend\Mvc\MvcEvent;
use Zend\Permissions\Acl\Exception\ExceptionInterface as AclException;
use Zend\Session\Container;
use Zend\View\Helper\Navigation\AbstractHelper as NavigationHelper;

/**
 * Initializes the ltkm module and makes sure that authentication is required
 *
 * @package User
 */
class Module implements ConfigProviderInterface
{

    /**
     * This makes sure authentication is required on every route that is not in the white list
     *
     * <p>This is done by adding route event listener to the event manager
     * with high priority.</p>
     * <p>In unit tests this event is removed based on the fact that this has
     * priority of -100.</p>
     *
     * @param MvcEvent $e
     */
    public function onBootstrap(MvcEvent $e)
    {
        $eventManager = $e->getApplication()->getEventManager();;
        $eventManager->attach(MvcEvent::EVENT_ROUTE, array($this, 'protectPage'), -100);
    }

    public function protectPage(MvcEvent $event)
    {
        $match = $event->getRouteMatch();
        if (!$match || Console::isConsole()) {
            // we cannot do anything without a resolved route
            // or in the console
            return null;
        }
        $controller = $match->getParam('controller');
        $action = $match->getParam('action');
        $namespace = $match->getParam('__NAMESPACE__');

        if ($namespace === null) {
            $parts = explode('\\', $controller);
            $moduleNamespace = $parts[0];
        } else {
            $parts = explode('\\', $namespace);
            $moduleNamespace = $parts[0];
        }

        $services = $event->getApplication()->getServiceManager();
        $config = $services->get('config');

        /** @var AuthenticationService $auth */
        $auth = $services->get('auth');
        $acl = $services->get('acl');

        // get the role of the current user
        /** @var MAPersonInterface $currentUser */
        $currentUser = $services->get('user');
        if (!$currentUser instanceof MAPersonInterface) {
            if ($auth->hasIdentity()) {
                $currentUser = $auth->getIdentity();
            }
            if (!$currentUser instanceof MAPersonInterface) {
                echo "Unable to login to kotka."; exit();
            }
        }
        $role = $currentUser->getMARoleKotka();
        if (is_array($role)) {
            $role = current($role);
            $currentUser->setMARoleKotka($role);
        }
        // This is how we add default acl and role to the navigation view helpers
        NavigationHelper::setDefaultAcl($acl);
        NavigationHelper::setDefaultRole($role);

        // check if the current module wants to use the ACL
        $aclModules = $config['acl']['modules'];
        if (!empty($aclModules) && !in_array($moduleNamespace, $aclModules)) {
            return null;
        }

        // Get the short name of the controller and use it as resource name
        $resourceAliases = $config['acl']['resource_aliases'];
        if (isset($resourceAliases[$controller])) {
            $resource = $resourceAliases[$controller];
        } else if (isset($resourceAliases[$moduleNamespace])) {
            $resource = $resourceAliases[$moduleNamespace];
        } else {
            $resource = strtolower(substr($controller, strrpos($controller, '\\') + 1));
        }

        // If a resource is not in the ACL add it
        if (!$acl->hasResource($resource)) {
            $acl->addResource($resource);
        }
        try {
            if ($acl->isAllowed($role, $resource, $action)) {
                return null;
            }
        } catch (AclException $ex) {
            /** @var \Zend\Log\Logger $log */
            $log = $services->get('logger');
            $log->alert($ex->getMessage());
        }

        // If the role is not allowed access to the resource we have to redirect the
        // current user to the log in page.
        $e = new EventManager('user');
        $e->trigger('deny', $this, array(
                'match' => $match,
                'role' => $role,
                'acl' => $acl
            )
        );
        return $this->redirectToLogin($event, $role);
    }

    private function redirectToLogin(MvcEvent $event, $role) {
        // Gets the router object
        $router = $event->getRouter();
        $container = new Container(UserController::REDIRECT_CONTAINER_NAME);
        $container->redirect = $event->getRequest()->getUri();
        $loginPage = $router->assemble(array(), array('name' => 'https/user'));

        $response = $event->getResponse();
        if ($response instanceof Response) {
            $response->getHeaders()->addHeaderLine('Location', $loginPage);
            $role !== User::ROLE_GUEST ? $response->setStatusCode(403) : $response->setStatusCode(302);
        }
        return $response;
    }


    /**
     * Returns the module config array
     * @return array|mixed|\Traversable
     */
    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }
}
